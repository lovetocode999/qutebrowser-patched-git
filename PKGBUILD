# Maintainer: Florian Bruhin (The Compiler) <archlinux.org@the-compiler.org>
# Contributor: Elijah Gregg <lovetocode999@gmail.com>
# Contributor: Morten Linderud <foxboron@archlinux.org>
# Contributor: Pierre Neidhardt <ambrevar@gmail.com>
# vim: set ts=4 sw=4 et ft=sh:

pkgname=qutebrowser-git-patched
pkgver=1.13.1.r640.g5a78bffb1
pkgrel=1
pkgdesc="A keyboard-driven, vim-like browser based on PyQt5"
arch=("any")
url="https://www.qutebrowser.org/"
license=("GPL")
depends=("python-attrs" "python-jinja" "python-pygments" "python-pypeg2"
         "python-pyqt5" "python-yaml" "qt5-base" "python-pyqtwebengine"
         "python-setuptools")
makedepends=("asciidoc" "pygmentize" "git")
optdepends=("gst-libav: media playback with qt5-webkit backend"
            "gst-plugins-base: media playback with qt5-webkit backend"
            "gst-plugins-good: media playback with qt5-webkit backend"
            "gst-plugins-bad: media playback with qt5-webkit backend"
            "gst-plugins-ugly: media playback with qt5-webkit backend"
            "pdfjs: displaying PDF in-browser"
            "qt5-webkit: alternative backend")
options=(!emptydirs)
conflicts=('qutebrowser')
provides=('qutebrowser')
source=('git+https://github.com/qutebrowser/qutebrowser.git')
sha256sums=('SKIP')

pkgver() {
    cd "$srcdir/qutebrowser"
    # Minor releases are not part of the master branch
    _tag=$(git tag --sort=v:refname | tail -n1)
    printf '%s.r%s.g%s' "${_tag#v}" "$(git rev-list "$_tag"..HEAD --count)" "$(git rev-parse --short HEAD)"
}

build() {
    cd "$srcdir/qutebrowser"
    # Merge non-problematic PRs first
    git fetch origin pull/3691/head:pr3691
    git merge --no-edit pr3691
    git fetch origin pull/4545/head:pr4545
    git merge --no-edit pr4545
    git fetch origin pull/5046/head:pr5046
    git merge --no-edit pr5046
    git fetch origin pull/5326/head:pr5326
    git merge --no-edit pr5326
    git fetch origin pull/5261/head:pr5261
    git merge --no-edit pr5261
    git fetch origin pull/5338/head:pr5338
    git merge --no-edit pr5338
    git fetch origin pull/5481/head:pr5481
    git merge --no-edit pr5481
    git fetch origin pull/5638/head:pr5638
    git merge --no-edit pr5638
    # Then force rebase and merge PRs with conflicts
    git fetch origin pull/4477/head:pr4477
    git checkout pr4477
    git rebase -Xtheirs master
    git checkout master
    git merge --no-edit pr4477
    # And finally build qutebrowser
    python scripts/asciidoc2html.py
    a2x -f manpage doc/qutebrowser.1.asciidoc
    python setup.py build
}

package() {
    cd "$srcdir/qutebrowser"
    make -f misc/Makefile DESTDIR="$pkgdir" PREFIX=/usr install
}
